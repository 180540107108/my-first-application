package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText etFirstName, etLastName;
    ImageView ivClose;
    Button btnSubmit;
    TextView tvDisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etFirstName = findViewById(R.id.etActFirstName);
        etLastName = findViewById(R.id.etActlastName);

        ivClose = findViewById(R.id.ivActClose);

        btnSubmit = findViewById(R.id.btnActSubmit);

        tvDisplay = findViewById(R.id.tvActDisplay);

        btnSubmit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                 startActivity(new Intent(MainActivity.this,SecondActivity.class));
                 finish();
            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View view) {
                                           finish();
                                       }
                                   }
        );
                }
                }